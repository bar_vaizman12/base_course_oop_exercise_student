package AerialVehicles;


import Entities.Coordinates;

public abstract class AerialVehicle {

    protected int hoursFromFix = 0 ;
    protected String status ;

    public abstract String getName();


    public int getHoursFromFix() {
        return hoursFromFix;
    }

    public void setHoursFromFix(int hoursFromFix) {
        this.hoursFromFix = hoursFromFix;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void flyTo(Coordinates coordinates){
        if(this.status == "ready"){
            System.out.println("Flying to: < " + coordinates.getLatitude() +","+ coordinates.getLongitude() + " >");
            this.setStatus("in Fly");
        }
        else {
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    };

    public void land(Coordinates coordinates){
        System.out.println("Landing on: < " + coordinates.getLatitude() +","+ coordinates.getLongitude() +" >");
        this.check();
    };
    public abstract void check();

    public void repair(){
        this.setHoursFromFix(0);
        this.setStatus("ready");
    };
}
