package AerialVehicles;


public class F16 extends Fighters implements BdaAbility{

    private cameraTypes typeCamera ;

    public F16(int numOfRocket, rocketTypes typeRocket , cameraTypes typeCamera) {
        super(numOfRocket, typeRocket);
        this.typeCamera = typeCamera ;
    }


    public  String getName() {
        return "F16";
    }


    @Override
    public cameraTypes camera() {
        return typeCamera;
    }
}
