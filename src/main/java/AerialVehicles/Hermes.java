package AerialVehicles;

public abstract class Hermes extends katmam implements BdaAbility , IntellgenceAbility{

    private static final int hoursToFix = 100;
    private sensorTypes typeSensor ;
    private cameraTypes typeCamera ;

    public Hermes(sensorTypes typeSensor, cameraTypes typeCamera) {
        this.typeSensor = typeSensor;
        this.typeCamera = typeCamera;
    }

    @Override
    public void check() {
        if (this.getHoursFromFix()>= hoursToFix){
            this.setStatus("not ready");
            this.repair();
        }
        else {
            this.setStatus("ready");
        }
    }

    @Override
    public sensorTypes sensor() {
        return typeSensor;
    }

    @Override
    public cameraTypes camera() {
        return typeCamera;
    }
}
