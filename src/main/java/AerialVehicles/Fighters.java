package AerialVehicles;

public abstract class Fighters extends AerialVehicle implements AttackAbility{

    private static final int hoursToFix = 250;
    private int numOfRocket ;
    private rocketTypes typeRocket ;

    public Fighters(int numOfRocket, rocketTypes typeRocket) {
        this.numOfRocket = numOfRocket;
        this.typeRocket = typeRocket;
    }

    @Override
    public void check() {
        if (this.getHoursFromFix()>= hoursToFix){
            this.setStatus("not ready");
            this.repair();
        }
        else {
            this.setStatus("ready");
        }
    }

    @Override
    public int rocketAmount() {
        return this.numOfRocket;
    }

    @Override
    public rocketTypes rocket() {
        return typeRocket;
    }
}
