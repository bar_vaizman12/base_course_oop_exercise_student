package AerialVehicles;

public interface AttackAbility {

    public static enum rocketTypes{
        PYTHON ,
        AMRAM ,
        SPICE250
    }

    int rocketAmount();
    rocketTypes rocket();
}
