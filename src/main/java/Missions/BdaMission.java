package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.BdaAbility;
import Entities.Coordinates;

public class BdaMission extends Mission{



    private String objective ;

    public BdaMission(Coordinates coordinates, String namePilot, AerialVehicle aerialVehicle , String objective) throws AerialVehicleNotCompatibleException {
        super(coordinates, namePilot, aerialVehicle);
        if (aerialVehicle instanceof BdaAbility){
            this.objective = objective ;
        }
        else {
            throw new AerialVehicleNotCompatibleException("an aircraft is not suitable for the mission");
        }

    }

    @Override
    public String executeMission() {
        String message = this.namePilot + " : " +this.getAerialVehicle().getName() + " taking pictures of " + this.objective + " with : "
                +((BdaAbility)this.getAerialVehicle()).camera();
        return message;
    }
}
