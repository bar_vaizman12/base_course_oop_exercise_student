import AerialVehicles.*;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntellgenceMission;

public class Main {

    public static void main(String[] args) throws AerialVehicleNotCompatibleException {
        F15 f15 = new F15(20 , AttackAbility.rocketTypes.AMRAM , IntellgenceAbility.sensorTypes.ELINT);
        Zik zik = new Zik(IntellgenceAbility.sensorTypes.INFRARED , BdaAbility.cameraTypes.REGULAR);
        Kochav kochav = new Kochav(IntellgenceAbility.sensorTypes.INFRARED , BdaAbility.cameraTypes.REGULAR , 20 , AttackAbility.rocketTypes.AMRAM);
        Coordinates coordinates = new Coordinates(5.5 , 34.5);
        AttackMission attackMission = new AttackMission(coordinates,"bar" , f15 , "suria") ;
        BdaMission bdaMission = new BdaMission(coordinates , "shay" , kochav , "place");
        IntellgenceMission intellgenceMission = new IntellgenceMission(coordinates , "yohav" , zik , "gaza");
        System.out.println(attackMission.executeMission());
        System.out.println(bdaMission.executeMission());
        System.out.println(intellgenceMission.executeMission());
    }


}
